<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="Shop.domain.Item" %>
<%@ page import="java.util.List" %>
<%@ page import="Shop.lists.Shop" %>
<%@ page import="File.FileManager" %>
<%--
  Created by IntelliJ IDEA.
  User: Пользователь
  Date: 18.10.2020
  Time: 9:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SHOP</title>
    <style>
        .items{
            display: flex;

        }

        .items>div{
            border: red 2px solid;
            width: 300px;

        }
    </style>
</head>
<body>

<center><h1><c:out value="${pageContext.request.getAttribute('category')}"/></h1>
</center>
    <c:set var="shop" value="${pageContext.request.getAttribute('shop')}"/>


<div class="items">
    <c:forEach var="item" items="${shop.getItems()}">
        <div>
           <h3> <c:out value="${item.getNameOfItem()}"/> </h3>
            <h5><c:out value="${item.getPrice()}" /></h5>
            <form method="post" action="${pageContext.request.contextPath}/card">
                <input type="submit">
                <input hidden name="item" value="${item.getNameOfItem()}">
                <input hidden name="category" value="${pageContext.request.getAttribute('category')}">
            </form>
        </div>
    </c:forEach>

    <form action="${pageContext.request.contextPath}/jsp/result.jsp" method="get">
        <input type="submit" value="buy">

    </form>
</div>

</body>
</html>
