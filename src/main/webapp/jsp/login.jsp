<%--
  Created by IntelliJ IDEA.

  Date: 17.10.2020
  Time: 12:02
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>LOGIN</title>
</head>
<body>



<c:if test="${pageContext.request.getAttribute('error')}">
    <h3><c:out value="${pageContext.request.getAttribute('error')}"/></h3>
</c:if>
<center>
<form method="post" action="${pageContext.request.contextPath}/authentication">
    Username  <input type="text" name="username"><br>
    Password<input type="password" name="pass"><br>
    <input type="submit" value="Log in">
</form>

<a href="jsp/singIn.jsp" >Sign in</a>
</center>
</body>
</html>
