package Servlelts;

import File.FileManager;
import Shop.domain.Item;
import Shop.lists.Shop;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ItemsServlet")
public class ItemsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String category = request.getParameter("category");
        String path = "C:\\Users\\Пользователь\\Desktop\\docs\\APJAVA\\assignments & practices & articles\\Assignment 7\\items.txt";
        Shop shop = new Shop();

        FileManager itemsFile = new FileManager(path);
        itemsFile.AddItemsToList(shop);



        request.setAttribute("shop",shop);
        request.setAttribute("category",category);


        for(Item item : shop.getItems()){
            System.out.println(item.getNameOfItem());
        }

        request.getRequestDispatcher("/jsp/category.jsp").forward(request,response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
