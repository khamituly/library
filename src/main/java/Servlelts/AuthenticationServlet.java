package Servlelts;

import File.FileManager;
import User.domain.User;
import User.domain.UserList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet(name = "firstServlet")
public class AuthenticationServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       String path = "C:\\Users\\Пользователь\\Desktop\\docs\\APJAVA\\assignments & practices & articles\\Assignment 7\\users.txt" ;
       UserList userList = new UserList();
       FileManager userFile = new FileManager(path);
       userFile.AddUsersToList(userList);
        String username,pass;

        if(request.getAttribute("username")==null) {
            username = request.getParameter("username");
            pass = request.getParameter("pass");
        }else{
            username = (String) request.getAttribute("username");
            pass = (String) request.getAttribute("pass");
        }


        for(User user : userList.getUserList()){
            if(user.getUsername().equals(username) && user.getPassword().equals(pass)){
                Cookie cookie = new Cookie("name", user.getName());
                cookie.setMaxAge(-1);
                response.addCookie(cookie);

                request.getRequestDispatcher("/jsp/items.jsp").forward(request,response);
            }else{
                request.setAttribute("error","invalid username or password");
                request.getRequestDispatcher("/jsp/login.jsp").forward(request,response);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
