package Servlelts;

import File.FileManager;
import Shop.domain.Item;
import Shop.lists.Cart;
import Shop.lists.Shop;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "CartServlet")
public class CartServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        Cart cart = new Cart();

        String name = request.getParameter("item");
        String path = "C:\\Users\\Пользователь\\Desktop\\docs\\APJAVA\\assignments & practices & articles\\Assignment 7\\items.txt" ;

        FileManager file = new FileManager(path);
        Shop shop = new Shop();
        file.AddItemsToList(shop);

        Item target = new Item();
        for(Item item:shop.getItems()){
            if(item.getNameOfItem().equals(name)){
                target = item;
            }
        }

        cart.getItemsList().add(target);

        session.setMaxInactiveInterval(60);
        session.setAttribute("cart",cart);

        request.setAttribute("category",request.getParameter("category"));
        request.setAttribute("shop",shop);
        request.getRequestDispatcher("/jsp/category.jsp").forward(request,response);
        
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
