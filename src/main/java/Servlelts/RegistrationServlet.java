package Servlelts;

import File.FileManager;
import User.domain.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "RegistrationServlet")
public class RegistrationServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       String path = "C:\\Users\\Пользователь\\Desktop\\docs\\APJAVA\\assignments & practices & articles\\Assignment 7\\users.txt";
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String username = request.getParameter("username");
        String pass = request.getParameter("pass");

        User user = new User(name,username,email,pass);

        FileManager fileManager = new FileManager(path);
        fileManager.AddUser(user);

        request.setAttribute("username",user.getUsername());
        request.setAttribute("pass", user.getPassword());
        request.getRequestDispatcher("/jsp/login.jsp").forward(request,response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
