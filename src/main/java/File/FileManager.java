package File;

import Shop.domain.Item;
import File.interfaces.IFileManager;
import Shop.lists.Shop;
import User.domain.User;
import User.domain.UserList;

import java.io.*;
import java.util.Scanner;

public class FileManager implements IFileManager {
    private String path;

    public FileManager(String path){
        setPath(path);
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public void AddUsersToList(UserList userList) throws FileNotFoundException {
        File file =new File(path);
        Scanner sc = new Scanner(file);
        String name,email,username,password;
        
        while (sc.hasNext()){
            name = sc.next();
            username = sc.next();
            email =sc.next();
            password = sc.next();

            User user = new User(name,username,email,password);
            userList.addUser(user);
        }
    }

    @Override
    public void AddItemsToList(Shop shop) throws FileNotFoundException,NullPointerException  {
        File file = new File(path);
        Scanner sc = new Scanner(file);
        String name,link;
        int index,price;
        while(sc.hasNext()){
            index = sc.nextInt();
            name = sc.next();
            link = sc.next();
            price = sc.nextInt();

            Item item = new Item(index,name,link,price);
            shop.addItem(item);

        }
    }

    @Override
    public void AddUser(User user) throws IOException {
        File file = new File(path);
        FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
        BufferedWriter writer = new BufferedWriter(fw);

        writer.append(" ");
        writer.write(user.getName());
        writer.append(" ");
        writer.write(user.getUsername());
        writer.append(" ");
        writer.write(user.getEmail());
        writer.append(" ");
        writer.append(user.getPassword());
        writer.append('\n');

        writer.close();
    }
}
