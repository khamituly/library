package File.interfaces;

import Shop.lists.Shop;
import User.domain.User;
import User.domain.UserList;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface IFileManager {
    public void AddUsersToList(UserList userList) throws FileNotFoundException;
    public void AddItemsToList(Shop shop) throws FileNotFoundException;
    public void AddUser(User user) throws IOException;
}
