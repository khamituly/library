package Shop.domain;

public class Item {
    private int id;
    private int indexOfCategory;
    private static int idCount = 0;
    private String nameOfItem;
    private String linkToPicture;
    private int price;

    public Item(){}

    public Item(int indexOfCategory,String nameOfItem, String linkToPicture, int price) {
        this.indexOfCategory = indexOfCategory;
        this.nameOfItem = nameOfItem;
        this.linkToPicture = linkToPicture;
        this.price = price;
        this.id = idCount++;
    }

    public int getIndexOfCategory() {
        return indexOfCategory;
    }

    public void setIndexOfCategory(int indexOfCategory) {
        this.indexOfCategory = indexOfCategory;
    }

    public String getNameOfItem() {
        return nameOfItem;
    }

    public void setNameOfItem(String nameOfItem) {
        this.nameOfItem = nameOfItem;
    }

    public String getLinkToPicture() {
        return linkToPicture;
    }

    public void setLinkToPicture(String linkToPicture) {
        this.linkToPicture = linkToPicture;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
