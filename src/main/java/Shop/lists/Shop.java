package Shop.lists;

import Shop.domain.Item;

import java.util.ArrayList;
import java.util.List;



public  class Shop {
    List<Item> items;

    public Shop(){
        this.items = new ArrayList<>();
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public void addItem(Item item){
        this.items.add(item);
    }

    public void removeItem(Item item){
        this.items.remove(item);
    }
}
