package Shop.lists;

import Shop.domain.Item;

import java.util.ArrayList;
import java.util.List;

public class Cart {
    private List<Item> itemList;

    public Cart() {
        this.itemList = new ArrayList<>();
    }

    public List<Item> getItemsList() {
        return itemList;
    }

    public void setItemsList(List<Item> itemList) {
        this.itemList = itemList;
    }


}
