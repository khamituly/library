package User.interrfaces;

import User.domain.User;

public interface IUserController {
    void addUser(User user);
    void removeUser(User user);
}
