package User.domain;

import User.interrfaces.IUserController;

import java.util.ArrayList;
import java.util.List;

public class UserList implements IUserController {
    private List<User> userList;

    public UserList(){
        this.userList = new ArrayList<>();
    }

    public UserList(List<User> userList) {
        this.userList = userList;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }


    @Override
    public void addUser(User user) {
        this.userList.add(user);
    }

    @Override
    public void removeUser(User user) {
        for(User target:userList){
            if(target == user){
                userList.remove(target);
            }
        }
    }
}
